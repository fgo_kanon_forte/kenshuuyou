﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PutOn_Delete : MonoBehaviour
{
    [SerializeField] private GameObject placementPrefab;
    [SerializeField] private Camera arCamera;
    [SerializeField] private ARRaycastManager raycastManager;
    private List<ARRaycastHit> raycastHits = new List<ARRaycastHit>();
    private GameObject spawnedObject;

    private void Update()
    {
        if (Input.touchCount <= 0) return; //タッチされなければUpdate()メソッドから追い出される。


        //タッチされれば、下記の処理を順に実行
        var touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Began)//普通のタッチ処理
        {
            // arCameraカメラからrayを照射
            var ray = arCamera.ScreenPointToRay(touch.position);
            // Cubeをタップした場合は削除する
            RaycastHit hit; //レイの当たった場所の数値が入る。

            //オブジェクトに当たったか、当たってないかの真偽
            //フィジックス.レイキャスト（レイ飛ばした、どこ当たった？)
            //フィジックス（物理）
            bool hasHit = Physics.Raycast(ray, out hit);

            if (hasHit)
            {
                //もしオブジェクトに当たった場合は・・・？
                var target = hit.collider.gameObject;
                //☆１は何？ ターゲットにするのは("Cube")
                if (target.name.Contains("Cube"))
                {
                    Destroy(target);
                    //それを破壊していく。
                    return;
                }

                //オブジェクトに当たっていない場合は・・・？
                //キューブを配置
                //照射したRay,戻ってきたRay,検知タイプ
                var isHitPlane = raycastManager.Raycast(ray, raycastHits, TrackableType.PlaneWithinPolygon);
                if (isHitPlane)
                {
                    // 複数のPlaneがあった場合、最も近いPlaneが0番目に入っている
                    Pose pose = raycastHits[0].pose;
                    // 配置すべき座標
                    Vector3 placePosition = pose.position;
                    //オブジェクトの生成
                    spawnedObject = Instantiate(placementPrefab, placePosition, Quaternion.identity);
                }
            }
        }
    }
}

