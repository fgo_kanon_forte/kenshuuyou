﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class Delete_or_Move_Ver8 : MonoBehaviour
{
    [SerializeField] private GameObject[] spawnCubes = new GameObject[5]; //生成物1~5

    [SerializeField] private Camera arCamera; //端末のカメラ
    [SerializeField] private ARRaycastManager ARSessionOrigin;

    private List<ARRaycastHit> raycastHits = new List<ARRaycastHit>();
    private List<GameObject> spawnList = new List<GameObject>(); //召喚した豆腐を配列していく。

    private GameObject catchBox; //持ったオブジェクトを維持する変数

    //制御
    private bool Mode_Change = true; //true = 移動モード ： false = 削除モード
    private string color = "blue"; //色を変える鍵 初期は青

    //------------------------------------------------------------

    //     下記のメソッドは   Mode_Change.cs   で制御する

    public string LookColor(){return color;}
    public void ColorChanger()
    {
        switch(color)
        {
            case "blue":
                color = "red";
                break;
            case "red":
                color = "green";
                break;
            case "green":
                color = "yellow";
                break;
            case "yellow":
                color = "purple";
                break;
            case "purple":
                color = "blue";
                break;
        }
    }

    public bool LookChangeCount(){return Mode_Change;}
    public void Changer()
    {
        // if(Mode_Change == true)
        // { Mode_Change = false; }
        // else
        // { Mode_Change = true; }

        // // 1
        // if (Mode_Change == true)
        //     Mode_Change = false;
        // else
        //     Mode_Change = true;
        // // 2
        // if (Mode_Change == true) {
        //     Mode_Change = false;
        // } else {
        //     Mode_Change = true;
        // }

        //3
        if (Mode_Change == true)
        {
            Mode_Change = false;
        }
        else
        {
            Mode_Change = true;
        }
    }

    //     上記のメソッドは   Mode_Change.cs   で制御する

    //------------------------------------------------------------

    //オブジェクトを移動、生成するときに必要な座標を返すメソッド
    private Vector3 Move_Touch_Vector3()
    {
        //オブジェクトを召喚、移動するときの座標を指定する
        Pose pose = raycastHits[0].pose;
        Vector3 placePosition = pose.position + new Vector3(0.00f,0.05f,0.00f);

        return placePosition;
    }

    void Update()
    {
        //画面触れてなければUpdate()を抜ける
        if(Input.touchCount <= 0)
        {
            return;
        }

        //触られた判定が入ったことで、触った場所に向かってRayを放つ
        var ray = arCamera.ScreenPointToRay(Input.GetTouch(0).position);

        //レイの当たった場所の数値が入る。
        RaycastHit hit;

        //Rayを飛ばした部分に物理判定が無い場合Updateを抜ける
        if(!Physics.Raycast(ray, out hit))
        {
            return;
        }

        //----- ↓ -----タッチの分岐----- ↓ -----

        //普通のタッチ処理
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            //衝突した場所がARPlaneだったら・・・
            if (hit.collider.gameObject.name.Contains("ARPlane"))
            {
                if(!ARSessionOrigin.Raycast(ray, raycastHits, TrackableType.PlaneWithinPolygon))
                {
                    return;
                }
                //オブジェクトを設置する場所を指定するメソッド
                Vector3 objectPosition = Move_Touch_Vector3();

                //colorの中にある文字列によって生成するオブジェクトの色を変える。
                //生成したオブジェクトをlistに格納
                switch(color)
                {
                    case "blue":
                        spawnList.Add(Instantiate(spawnCubes[0] , objectPosition , Quaternion.identity));
                        break;
                    case "red":
                        spawnList.Add(Instantiate(spawnCubes[1] , objectPosition , Quaternion.identity));
                        break;
                    case "green":
                        spawnList.Add(Instantiate(spawnCubes[2] , objectPosition , Quaternion.identity));
                        break;
                    case "yellow":
                        spawnList.Add(Instantiate(spawnCubes[3] , objectPosition , Quaternion.identity));
                        break;
                    case "purple":
                        spawnList.Add(Instantiate(spawnCubes[4] , objectPosition , Quaternion.identity));
                        break;
                }
                //格納されたものを選択したものとしてcatchBoxに代入
                catchBox = spawnList[spawnList.Count-1];
            }
            //生成したオブジェクトに触れたとき・・・（オブジェクトの名前は部分統一する。）
            else if(hit.collider.gameObject.name.Contains("Cube"))
            {
                //ARPlane以外の当たったオブジェクトをcatchBoxへ格納
                catchBox = hit.collider.gameObject;

                //削除モード：false の時・・・
                if(!Mode_Change)
                {
                    //Rayを飛ばしたときに、キューブに当たればそれを消す。
                    Destroy(catchBox);
                }
                return;
            }
        }
        //選択した物体を動かす処理
        //もし・・・ 指を動かしたら ＋ 移動モード ＋ 保持してるオブジェクトがnullではないとき
        else if(Input.GetTouch(0).phase == TouchPhase.Moved && Mode_Change && catchBox != null)
        {
            //オブジェクトを移動するときに指がオブジェクトに当たっていないときのエラーを防ぐ。
            if(!ARSessionOrigin.Raycast(ray, raycastHits, TrackableType.PlaneWithinPolygon))
            {
                return;
            }

            //オブジェクトを設置する場所を指定するメソッド
            Vector3 objectPosition = Move_Touch_Vector3();
            //保持されたオブジェクトの場所を移動します。
            catchBox.transform.position = objectPosition;
        }
        //画面から指を離したとき、catchBoxを空にする。
        else if(Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            catchBox = null;
        }
    }
}
