﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PutOn_Moved : MonoBehaviour
{
    [SerializeField] private GameObject placementPrefab;
    [SerializeField] private Camera arCamera;
    [SerializeField] private ARRaycastManager raycastManager;
    private List<ARRaycastHit> raycastHits = new List<ARRaycastHit>();

    private List<GameObject> spawnList = new List<GameObject>(); //召喚した豆腐を配列していく。
    private GameObject spawnedObject;

    private int listCounter = 0;
    private int num=0;

    private int Mode_Change = 0;
    public Text text;

    public void ButtonClick()
    {
        Mode_Change++;
        Debug.Log("Log4:"+Mode_Change);
        if(Mode_Change > 2){
            Mode_Change = 0;
        }
    }

    void Update()
    {
        if(Mode_Change % 2 == 1)
        {
            BoxDeleteMode();
            text.text = "削除モード";
        }
        else if(Mode_Change % 2 == 0)
        {
            BoxMoveMode();
             text.text = "移動モード";
        }

        if(Mode_Change >= 2){
            Mode_Change = 0;
        }
    }


    private void BoxMoveMode()
    {
        if (Input.touchCount <= 0) return; //タッチされなければUpdate()メソッドから追い出される。

        //タッチされれば、下記の処理を順に実行
        var touch = Input.GetTouch(0);
        // arCameraカメラからrayを照射
        var ray = arCamera.ScreenPointToRay(touch.position);
        RaycastHit hit; //レイの当たった場所の数値が入る。

        //オブジェクトに当たったか、当たってないかの真偽
        //フィジックス.レイキャスト（レイ飛ばした、どこ当たった？)
        //フィジックス（物理）
        bool hasHit = Physics.Raycast(ray, out hit);

        //モノを置く場所を検知 真 偽
        var isHitPlane = raycastManager.Raycast(ray, raycastHits, TrackableType.PlaneWithinPolygon);
        //もしオブジェクトに当たった場合は・・・？
        var target = hit.collider.gameObject; //当たったオブジェクトをタゲにする
        Pose pose = raycastHits[0].pose;
        Vector3 placePosition = pose.position;

        int index = spawnList.IndexOf(target);
        if(index>=0){
            num = index;
        }else if(index <= -1){
            //処理はしない
        }

        if (touch.phase == TouchPhase.Began)//普通のタッチ処理
        {
            if (hasHit)
            {
                 if (isHitPlane && !target.name.Contains(index.ToString())) 
                {
                    /// 置ける場所検知します。
                    /// キューブがない場所に触れると index = -1
                    /// -1が存在しないとき、オブジェクトを生成するための処理が始まる。

                    //オブジェクトの生成
                   // Debug.Log("Log1:" + target.name);
                    Debug.Log("Log2:" + index.ToString());
                    GameObject item = Instantiate(placementPrefab, placePosition, Quaternion.identity);
                    //生成するものをGameObject itemとして格納する
                    item.name = listCounter.ToString();
                    //itemの名前を番号で指定
                    spawnList.Add(item);
                    //0.1.2.3.4....10と番号で格納されていく
                    listCounter++;
                }
            }
        }
        else if(touch.phase == TouchPhase.Moved)
        {
                //移動したい。
                spawnList[num].transform.position = placePosition;
                Debug.Log("Log3:" + spawnList[num] + "を触ってます");
        }
    }

    private void BoxDeleteMode(){
        if (Input.touchCount <= 0) return; //タッチされなければUpdate()メソッドから追い出される。


        //タッチされれば、下記の処理を順に実行
        var touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Began)//普通のタッチ処理
        {
            // arCameraカメラからrayを照射
            var ray = arCamera.ScreenPointToRay(touch.position);
            // Cubeをタップした場合は削除する
            RaycastHit hit; //レイの当たった場所の数値が入る。

            //オブジェクトに当たったか、当たってないかの真偽
            //フィジックス.レイキャスト（レイ飛ばした、どこ当たった？)
            //フィジックス（物理）
            bool hasHit = Physics.Raycast(ray, out hit);

            if (hasHit)
            {
                //もしオブジェクトに当たった場合は・・・？
                var target = hit.collider.gameObject;
                //☆１は何？ ターゲットにするのは("Cube")
                if (target.name.Contains("Cube"))
                {
                    Destroy(target);
                    //それを破壊していく。
                    return;
                }

                //オブジェクトに当たっていない場合は・・・？
                //キューブを配置
                //照射したRay,戻ってきたRay,検知タイプ
                var isHitPlane = raycastManager.Raycast(ray, raycastHits, TrackableType.PlaneWithinPolygon);
                if (isHitPlane)
                {
                    // 複数のPlaneがあった場合、最も近いPlaneが0番目に入っている
                    Pose pose = raycastHits[0].pose;
                    // 配置すべき座標
                    Vector3 placePosition = pose.position;
                    //オブジェクトの生成
                    spawnedObject = Instantiate(placementPrefab, placePosition, Quaternion.identity);

                }
            }
        }
    }
}


