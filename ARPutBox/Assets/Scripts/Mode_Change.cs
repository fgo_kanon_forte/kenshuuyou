﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Mode_Change : MonoBehaviour
{
    [SerializeField] private GameObject ARSessionOrigin; //ヒエラルキーのオブジェクトのスクリプトを使いたい。
    [SerializeField] private Text textMode; //ボタンテキストの切り替え

    [SerializeField] private GameObject colorButton; //色を変えるボタン

    void Start() 
    {
        textMode.text = "移動モード";
    }

    public void onClick()
    {
        ARSessionOrigin.GetComponent<Delete_or_Move_Ver9>().Changer();
        bool Mode = ARSessionOrigin.GetComponent<Delete_or_Move_Ver9>().LookChangeCount();
        /// ボタンのクリック判定が入ったとき・・・
        /// ARSessionOriginのChanger()が起動して
        /// 移動モード、削除モードをboolで切り替えます。
        /// 
        /// /// 切り替わった状況を見て判定を返すのがLookChangeCount()
        /// ↓ 帰ってきたboolの値によって表示する文字を変える。

        if(Mode == true)
        {
            textMode.text = "移動モード";
        }
        else if(Mode == false)
        {
            textMode.text = "削除モード";
        }

    }

    public void onClickColor()
    {
        //７行目のGameObjectの中に代入されているオブジェクトのメソッドを使用する。
        //ColorChangerで生成する色を変えて、
        //colorでボタンの色を変更する
        ARSessionOrigin.GetComponent<Delete_or_Move_Ver9>().ColorChanger();
        string color = ARSessionOrigin.GetComponent<Delete_or_Move_Ver9>().LookColor();

        //LookColorの戻り値でボタンの色を制御
        switch(color)
        {
            case "blue":
                colorButton.GetComponent<Image>().color = Color.blue;
                break;
            case "red":
                colorButton.GetComponent<Image>().color = Color.red;
                break;
            case "green":
                colorButton.GetComponent<Image>().color = Color.green;
                break;
            case "yellow":
                colorButton.GetComponent<Image>().color = Color.yellow;
                break;
            case "purple":
                colorButton.GetComponent<Image>().color = Color.magenta;
                break;
            default:
                break;
        }
    }
}
