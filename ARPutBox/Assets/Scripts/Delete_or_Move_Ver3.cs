﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class Delete_or_Move_Ver3 : MonoBehaviour
{
    [SerializeField] private GameObject placementPrefab; //生成物
    [SerializeField] private Camera arCamera; //端末のカメラ
    [SerializeField] private ARRaycastManager raycastManager; //ARSessionOrigin
    private List<ARRaycastHit> raycastHits = new List<ARRaycastHit>();
    private List<GameObject> spawnList = new List<GameObject>(); //召喚した豆腐を配列していく。
    private GameObject catchBox;

    private bool Mode_Change = true;
    
    public void Changer()
    {
        if(Mode_Change == true)
        { Mode_Change = false; }
        else
        { Mode_Change = true; }
    }

    public bool LookChangeCount(){return Mode_Change;}
    
    void Update()
    {
        if(Input.touchCount <= 0) return; //タッチされなければUpdate()メソッドから追い出される。

        //タッチされれば、下記の処理を順に実行
        var touch = Input.GetTouch(0);
        // arCameraカメラからrayを照射
        var ray = arCamera.ScreenPointToRay(touch.position);
        RaycastHit hit; //レイの当たった場所の数値が入る。

        //オブジェクトに当たったか、当たってないかの真偽
        //フィジックス.レイキャスト（レイ飛ばした、どこ当たった？)
        //フィジックス（物理）
        
        bool hasHit = Physics.Raycast(ray, out hit);
        if(hasHit == false) return; //オブジェクトにRayが当たらなかったらUpdateを抜ける
        if(raycastManager.Raycast(ray, raycastHits, TrackableType.PlaneWithinPolygon) == false){return;} //モノを置く場所を検知 真 偽
        Pose pose = raycastHits[0].pose;
        Vector3 placePosition = pose.position + new Vector3(0.00f,0.05f,0.00f);
        //もしオブジェクトに当たった場合は・・・？
        var target = hit.collider.gameObject; //当たったオブジェクトをタゲにする

        int index = spawnList.IndexOf(target); //触ったオブジェクトのindex取得

        if (touch.phase == TouchPhase.Began)//普通のタッチ処理
        {
            if (hasHit)//Rayを飛ばした先に当たるとtrue
            {
                 if (target.name.Contains("ARPlane"))//平面検知かつ、刺さったRayが”Cube”以外だったら
                {                                                //オブジェクトを生成して、Listに入れる。
                    spawnList.Add(Instantiate(placementPrefab,  placePosition , Quaternion.identity));
                    catchBox = spawnList[spawnList.Count-1];
                    return;
                }
                else if(Mode_Change==false) //Mode_Changeがfalseの時、タップしたオブジェクトを消す
                {
                    //Rayを飛ばしたときに、キューブに当たればそれを消す。
                    Destroy(spawnList[index]);
                }
            }
            //掴んだ情報を保持するための変数
            catchBox = spawnList[index];
        }
        else if(touch.phase == TouchPhase.Moved && Mode_Change==true) //選択した物体を動かす処理
        {
                //保持した情報を使ってキューブを移動させる。
                Debug.Log("Log:"+placePosition);
                catchBox.transform.position = placePosition;
        }
        else if(touch.phase == TouchPhase.Ended)
        {
            //タッチを離した瞬間保持してた情報をnullにする。
            catchBox = null;
        }
    }
}


