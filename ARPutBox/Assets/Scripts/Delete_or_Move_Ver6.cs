﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class Delete_or_Move_Ver6 : MonoBehaviour
{
    private GameObject spawnCube1; //生成物1
    private GameObject spawnCube2; //生成物2
    private GameObject spawnCube3; //生成物3
    [SerializeField] private GameObject[] spawnCubes;
    [SerializeField] private Camera arCamera; //端末のカメラ
    [SerializeField] private ARRaycastManager raycastManager; //ARSessionOrigin

    private List<ARRaycastHit> raycastHits = new List<ARRaycastHit>();
    private List<GameObject> spawnList = new List<GameObject>(); //召喚した豆腐を配列していく。

    private GameObject catchBox; //持ったオブジェクトを維持する変数

    private bool Mode_Change = true;

    private string color = "blue";

    public void ColorChanger()
    {
        switch(color)
        {
            case "blue":
                color = "red";
                break;
            case "red":
                color = "green";
                break;
            case "green":
                color = "blue";
                break;
        }
    }
    public string LookColor(){return color;}

    public void Changer()
    {
        if(Mode_Change == true)
        { Mode_Change = false; }
        else
        { Mode_Change = true; }
    }

    public bool LookChangeCount(){return Mode_Change;}

    void start(){
        //召喚するオブジェクト情報を配列に代入
        spawnCubes = new GameObject[]{spawnCube1,spawnCube2,spawnCube3};
    }

    void Update()
    {
        //画面触れてなければUpdate()を抜ける
        if(Input.touchCount <= 0) return;

        //触られた判定が入ったことで、触った場所に向かってRayを放つ
        var ray = arCamera.ScreenPointToRay(Input.GetTouch(0).position);

        if(raycastManager.Raycast(ray, raycastHits, TrackableType.All) == false)return;

        //オブジェクトを召喚、移動するときの座標を指定する
        Pose pose = raycastHits[0].pose;
        Vector3 placePosition = pose.position + new Vector3(0.00f,0.05f,0.00f);

        //----- ↓ -----タッチの分岐----- ↓ -----

        //普通のタッチ処理
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
           
            //レイの当たった場所の数値が入る。
            RaycastHit hit;
          
            //Rayを飛ばした部分に物理判定が無い場合Updateを抜ける
            if(Physics.Raycast(ray, out hit) == false)return;
            

            //衝突した場所がARPlaneだったら・・・
            if (hit.collider.gameObject.name.Contains("ARPlane"))
            {
                switch(color)
                {   //生成したあとListに格納
                    case "blue":
                        spawnList.Add(Instantiate(spawnCubes[0],  placePosition , Quaternion.identity));
                        catchBox = spawnList[spawnList.Count-1];
                        break;
                    case "red":
                        spawnList.Add(Instantiate(spawnCubes[1],  placePosition , Quaternion.identity));
                        catchBox = spawnList[spawnList.Count-1];
                        break;
                    case "green":
                        spawnList.Add(Instantiate(spawnCubes[2],  placePosition , Quaternion.identity));
                        catchBox = spawnList[spawnList.Count-1];
                        break;
                }
                //格納されたものを選択したものとしてcatchBoxに代入
                
            }
            //ARPlane以外のオブジェクトに触れたとき・・・
            else if(hit.collider.gameObject.name.Contains("Cube")) 
            {
                //ARPlane以外の当たったオブジェクトをcatchBoxへ格納
                catchBox = hit.collider.gameObject;

                //削除モード：false の時・・・
                if(Mode_Change == false)
                {
                    //Rayを飛ばしたときに、キューブに当たればそれを消す。
                    Destroy(catchBox);
                }
            }
        }
        //選択した物体を動かす処理
        else if(Input.GetTouch(0).phase == TouchPhase.Moved && Mode_Change==true)
        {
            catchBox.transform.position = placePosition;
        }
        //画面から指を離したとき、catchBoxを空にする。
        else if(Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            catchBox = null;
        }
    }
}
