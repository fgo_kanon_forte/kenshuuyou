﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class My_Put_On_Box : MonoBehaviour
{
    [SerializeField] //インスペクタで編集したいです。
    // ↓  何を？  ↓
    public GameObject spawnedObject; //外部からオブジェクトを置く
    // ↑  これを  ↑

    public TrackableType type; //置く場所を検知する
    
    private ARRaycastManager raycastManager; //置くものの距離感をつかむ
    private List<ARRaycastHit> hitResults = new List<ARRaycastHit>();



    // Start is called before the first frame update
    void Start()
    {
        //コンポーネントにスクリプトを追加したときに一緒に追加する
        raycastManager = GetComponent<ARRaycastManager>();
    }

    // Update is called once per frame
    void Update()
    {   
        if(Input.touchCount > 0){
            // //どんなタッチ？
            Touch touch = Input.GetTouch(0);
            // var ray = arCamera.ScreenPointToRay(touch.position);
            // bool hasHit = Physics.Raycast(ray,hitResults);
            // if(touch.phase == TouchPhase.Began){
            // //普通のタッチがされました。
            // if(hasHit){
            //     var target = hitResults[0].collider.gameObject;
            //     if(target.name.Contains("Cube")){
            //         Destroy(target);
            //     }
            // }

                if(raycastManager.Raycast(touch.position,hitResults,TrackableType.PlaneWithinPolygon)){
                    //タッチされた場所、帰ってきたRay、どこを検知するか すべて取得
                    //そしてオブジェクトを配置する。どのように？
                    spawnedObject = Instantiate(spawnedObject,hitResults[0].pose.position,Quaternion.identity);
                    //このように ↑ オブジェクト・Rayの場所・オブジェクトの形や角度
                }
            }
        }
}