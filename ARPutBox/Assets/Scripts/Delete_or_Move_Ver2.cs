﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class Delete_or_Move_Ver2 : MonoBehaviour
{
    [SerializeField] private GameObject placementPrefab; //生成物
    [SerializeField] private Camera arCamera; //端末のカメラ
    [SerializeField] private ARRaycastManager raycastManager; //ARSessionOrigin
    private List<ARRaycastHit> raycastHits = new List<ARRaycastHit>();
    private List<GameObject> spawnList = new List<GameObject>(); //召喚した豆腐を配列していく。
    private GameObject catchBox;

    private bool Mode_Change = true;
    
    public void Changer()
    {
        if(Mode_Change == true)
        { Mode_Change = false; }
        else
        { Mode_Change = true; }
    }

    public bool LookChangeCount(){return Mode_Change;}
    
    void Update()
    {
        if(Input.touchCount <= 0) return; //タッチされなければUpdate()メソッドから追い出される。

        //タッチされれば、下記の処理を順に実行
        var touch = Input.GetTouch(0);
        // arCameraカメラからrayを照射
        var ray = arCamera.ScreenPointToRay(touch.position);
        RaycastHit hit; //レイの当たった場所の数値が入る。

        //オブジェクトに当たったか、当たってないかの真偽
        //フィジックス.レイキャスト（レイ飛ばした、どこ当たった？)
        //フィジックス（物理）
        
        Pose pose = raycastHits[0].pose;
        Vector3 placePosition = pose.position + new Vector3(0.00f,0.05f,0.00f);
         Debug.Log("一応画面触れたな・・・・？");

        if (touch.phase == TouchPhase.Began)//普通のタッチ処理
        {
            Debug.Log("Log:"+touch);
            bool hasHit = Physics.Raycast(ray, out hit);
            Debug.Log("Log:"+hasHit);
            if(hasHit == false) return; //オブジェクトにRayが当たらなかったらUpdateを抜ける 

            var target = hit.collider.gameObject; //当たったオブジェクトをタゲにする
            int index = spawnList.IndexOf(target); //触ったオブジェクトのindex取得
            if (hasHit)//Rayを飛ばした先に当たるとtrue
            {
                var isHitPlane = raycastManager.Raycast(ray, raycastHits, TrackableType.PlaneWithinPolygon); //モノを置く場所を検知 真 偽
                Debug.Log("Log:"+isHitPlane);
                if (isHitPlane && index == -1)
                /// 平面検知、飛ばしたRayがオブジェクトに当たらなかったら、true
                /// indexが-1なのは・・・
                /// 51行目の触ったオブジェクトが取得されない場合-1を返される。
                /// つまり、平面検知をして、触った先にオブジェクトがなければtrueを返す
                {
                    GameObject item = Instantiate(placementPrefab,  placePosition , Quaternion.identity);
                    spawnList.Add(item);
                    int item_No = spawnList.LastIndexOf(item);
                    spawnList[item_No].name = item_No.ToString();

                    /// ・生成物をGameObject型のitemとして格納します。
                    /// 
                    /// ・生成物(item)をspawnListに加えます。
                    /// 
                    /// ・「どの生成物を触ったか」を識別するために
                    ///   itemにindexと同じ名前を付けたい。
                    ///   LastIndexOf()で後方から検索できることを利用して
                    ///   新しく追加されたオブジェクトのindexを引き出す。
                    /// 
                    /// ・それをitemの名前にする。

                }
                else if(Mode_Change==false) //Mode_Changeがfalseの時、タップしたオブジェクトを消す
                {
                    //Rayを飛ばしたときに、キューブに当たればそれを消す。
                    Destroy(spawnList[index]);
                }
            }
            //掴んだ情報を保持するための変数
            catchBox = spawnList[index];
            Debug.Log("Log:"+catchBox);
        }
        else if(touch.phase == TouchPhase.Moved && Mode_Change==true) //選択した物体を動かす処理
        {
            //保持した情報を使ってキューブを移動させる。
            catchBox.transform.position = placePosition;
        }
        else if(touch.phase == TouchPhase.Ended)
        {
            //タッチを離した瞬間保持してた情報をnullにする。
            catchBox = null;
        }
    }
}


