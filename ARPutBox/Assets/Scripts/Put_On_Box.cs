﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

//スクリプトの中で、Requestコンポーネントを使って、
//ARRaycastManagerを指定してる？？
[RequireComponent(typeof(ARRaycastManager))]
public class Put_On_Box : MonoBehaviour
{
    //とりあえず使うものは？？
    [SerializeField]
    public GameObject spawnedObject; //置くもの(何を置くんだい？？)
    
    public TrackableType type; //置く場所を検知するため設定
    private ARRaycastManager raycastManager; //置くものの距離感を掴む

    // ↓ タッチした先からRayを出してぶつかった場所をList化してる？
    private List<ARRaycastHit> hitResults = new List<ARRaycastHit>();

    //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
    //豆腐を置くためにはRay(光線)を飛ばして距離感を図る必要がある     //
    //なので、Awake()メソッドの中で「Rayをコンポーネントに追加」を宣言//
    //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//

    //Start()メソッドでも意外と問題ない？？
    void Awake()
    {
        //スクリプトをコンポーネントに
        //追加したときに一緒に追加してくれる便利奴 ↓
        raycastManager = GetComponent<ARRaycastManager>();
        Debug.Log("Log.Awake:" + type);
    }

    // Update is called once per frame
    void Update()
    {
        // ↓ タッチ判定 ↓
        //MouseButtonではなくtouch系が好ましい
        if(Input.GetMouseButtonDown(0)){

             //Debug.Log("+-+-+-+-+-+-+-+TOUCH+-+-+-+-+-+-+-+");
             // Rayが貫通したポイント[0] , 帰ってきたRayの距離 , 検知設定はすべて対応
            if (raycastManager.Raycast(Input.GetTouch(0).position, hitResults, TrackableType.All))
            {
                //オブジェクトを生成(生成物 , Rayの距離.???.Rayの距離から座標指定？ , 回転.無回転)
                Instantiate(spawnedObject, hitResults[0].pose.position, Quaternion.identity);
                Debug.Log("Log.Update" + type);
            }
        }
    }
}
