﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Mode_Change_Ver2 : MonoBehaviour
{
    [SerializeField] public GameObject ARSessionOrigin; //ヒエラルキーのオブジェクトのスクリプトを使いたい。
    [SerializeField] public Text textMode; //移動削除テキストの切り替え

    public void onClick()
    {
        ARSessionOrigin.GetComponent<Delete_or_Move_Ver6>().Changer();
        bool Mode = ARSessionOrigin.GetComponent<Delete_or_Move_Ver6>().LookChangeCount();
        /// ボタンのクリック判定が入ったとき・・・
        /// ARSessionOriginのChanger()が起動して
        /// 移動モード、削除モードをboolで切り替えます。
        /// 
        /// /// 切り替わった状況を見て判定を返すのがLookChangeCount()
        /// ↓ 帰ってきたboolの値によって表示する文字を変える。

        if(Mode == true){
            textMode.text = "移動モード";
        }else if(Mode == false){
            textMode.text = "削除モード";
        }

    }
}
